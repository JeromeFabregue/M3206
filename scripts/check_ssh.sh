#!/bin/bash
if ( dpkg -s ssh | grep install > "Status: install ok installed" );
then
	echo "ssh: installé"
	
	if ( service ssh status | grep active > "Active: active *" );
		then
		echo "ssh: fonctionne "
	else 
		echo "ssh: le service n'est pas lancé"
		echo "ssh: lancement du service"
		/etc/init.d/sshd start
		echo "ssh: fonctionne"
	fi
else
	echo "[/!\] ssh: pas installé [/!\]"
	apt-get install ssh
	echo "ssh: est installé"
fi
