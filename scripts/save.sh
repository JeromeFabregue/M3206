#!/bin/bash

# Format

path=$(readlink -f $1) #Permet d'avoir le chemin absolu du dossier en paramètre

DATE=$(date +%Y_%m_%d_%H%M) #Variable contenant la Date et l'heure

echo $path > $path/path.txt #On copie le chemmin dans un fichier

TARTARGET=/tmp/backup/$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz  #Destination du backup

tar cvzf $TARTARGET $1 #On crée l'archive
