#!/bin/bash
if [ "$EUID" -ne 0 ];#$EUID contient le nom d'utilisateur de l'utilisateur actuel	 
then
	echo "[/!\] Vous devez être super-utilisateur [/!\]"
	exit;
else
	echo "Update database"
	apt-get update 1>> /dev/null
	echo "Upgrade system"
	apt-get upgrade 1>> /dev/null
fi
