#!/bin/bash

HISTFILE=~/.bash_history # Ces deux lignes permettent de passer l'interpréteur en mode interactif afin de pouvoir utiliser la commande history
set -o history

history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10 # Commande à exécuté avec ./...

