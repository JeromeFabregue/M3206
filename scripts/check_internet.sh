#!/bin/bash
echo "Checking internet connection"
wget -q --tries=10 --timeout=10 --spider http://google.com
#--timeout -> nombre de secondes limite avant que la connection soit considérée comme non fonctionnelle
#--tries -> nombre d'essais
#--spider -> vérifie la connectivité à la page sans la télécharger
if [[ $? -eq 0 ]]; then
	echo "Internet access OK"
else
	echo "[/!\] Not connected to Internet [/!\]"
	echo "[/!\] Please check configuration [/!\]"
fi

